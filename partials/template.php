<!DOCTYPE html>
<html>
<head>
	<title><?php get_title(); ?></title>

	<!-- BOOTSTRAP -->
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
	<!-- BOOTSWATCH -->
	<link rel="stylesheet" type="text/css" href="https://bootswatch.com/4/sandstone/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="../assets/styles/style.css">
</head>
<body>
	<nav class="navbar navbar-expand-lg navbar-dark bg-primary">
		<a class="navbar-brand" href="#">Kicks-Dict</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor01" aria-controls="navbarColor01" aria-expanded="false" aria-label="Toggle navigation">
		<span class="navbar-toggler-icon"></span>
		</button>

		<div class="collapse navbar-collapse" id="navbarColor01">
			<ul class="navbar-nav mr-auto">
				<li class="nav-item active">
					<a class="nav-link" href="catalog.php">Listings <span class="sr-only">(current)</span></a>
				</li>
				
				<?php  
					session_start();

					if(isset($_SESSION['user']) && $_SESSION['user']['role_id'] == 1){

				?>
					
					<li class="nav-item">
						<a class="nav-link" href="add-item.php">Add Item</a>
					</li>

				<?php
					}

					else{

				?>
					<li class="nav-item">
						<a class="nav-link" href="cart.php">Cart <span id="cartCount" class="badge bg-danger"><?php
		
							if(isset($_SESSION['cart'])){
								echo array_sum($_SESSION['cart']);
							}

							else{
								echo 0;
							}

						?></span></a>
					</li>
				
				<?php
					}

					if(isset($_SESSION['user'])){
				?>

					<li class="nav-item">
						<a href="../views/profile.php" class="nav-link">Hello <?php echo $_SESSION['user']['firstName'] ?>!</a>						
					</li>

					<li class="nav-item">
						<a href="../views/order-history.php" class="nav-link">Order History</a>
					</li>

					<li class="nav-item">
						<a href="../controllers/logout-process.php" class="nav-link">Logout</a>
					</li>

				<?php		
					}

					else{
				?>
					
					<li class="nav-item">
						<a href="login.php" class="nav-link">Login</a>
					</li>

					<li class="nav-item">
						<a href="register.php" class="nav-link">Register</a>
					</li>

				<?php
					}
				?>
			</ul>
		</div>
	</nav>

	<?php  
		get_body_contents();
	?>

	<!-- FOOTER -->
	<footer class="page-footer font-small bg-primary navbar-dark text-white">
		<div class="footer-copyright text-center py-3">© John Dominic Fandialan 2020</div>
	</footer>

	<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
	<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
</body>
</html>