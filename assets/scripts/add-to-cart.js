let addToCartBtns = document.querySelectorAll(".addToCart");

addToCartBtns.forEach(indiv_btn => {
	indiv_btn.addEventListener('click', btn=>{
		
		let id = btn.target.getAttribute("data-id");
		let quantity = btn.target.previousElementSibling.value;
		
		if(quantity <= 0){
			alert("Invalid quantity!");
		}

		else{
			let data = new FormData;
			data.append("id",id);
			data.append("cart", quantity);

			fetch("../../controllers/add-to-cart-process.php",{
				method: "POST",
				body: data
			})
			.then(response => {
				return response.text();
			})
			.then(res => {
				document.getElementById('cartCount').textContent = res;
			});
		}
	});
});