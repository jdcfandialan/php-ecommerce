<?php  
	require "connection.php";

	function validate_form(){
		$contactNumber = $_POST['contactNumber'];
		$errors = 0;

		if($contactNumber == ""){
			$errors++;
		}

		else if(strlen($contactNumber) != 11){
			$errors++;
		}

		if($errors > 0){
			return false;
		}

		else{
			return true;
		}
	}

	if(validate_form()){
		$contactNumber = $_POST['contactNumber'];
		$user_id = $_POST['user_id'];
		$isPrimary = 0;

		$contact_query = "INSERT INTO contacts(contactNo, user_id, isPrimary) VALUES ('$contactNumber', $user_id, $isPrimary)";
		$result = mysqli_query($connect, $contact_query);

		 header("Location: ../views/profile.php");
	}

	else{
		header("Location: ".$_SERVER['HTTP_REFERER']);
	}
?>