<?php  
	require "connection.php";

	session_start();

	$user_id = $_SESSION['user']['id'];
	$total = $_POST['totalPayment'];

	$payment_id = 0;
	$status_id = 0;

	if(isset($_POST['fromPayPal'])){
		$payment_id = 2;
		$status_id = 2;
	}

	if(isset($_POST['cod'])){
		$payment_id = 1;
		$status_id = 1;
	}
	
	$order_query = "INSERT INTO orders (total, user_id, payment_id, status_id) VALUES ($total, $user_id, $payment_id, $status_id)";

	$order_id = mysqli_insert_id($connect);

	$order = mysqli_query($connect, $order_query);

	foreach($_SESSION['cart'] as $item_id => $quantity){
		$item_query = "INSERT INTO item_order (item_id, order_id) VALUES ($item_id, $order_id, $quantity)";
		$result = mysqli_query($connect, $item_query);
	}

	unset($_SESSION['cart']);

	header("Location: ".$_SERVER['HTTP_REFERER']);
?>