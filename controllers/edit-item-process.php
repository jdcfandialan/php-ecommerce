<?php 
	require "connection.php";

	function validateForm(){

		$name = $_POST['name'];
		$price = $_POST['price'];
		$description = $_POST['description'];
		$category_id = $_POST['category'];

		//image handling
		$image = $_FILES['image'];
		$file_types = ['jpg','jpeg', 'png', 'gif','svg','webp','bmp','tiff','tif'];
		$file_ext = strtolower(pathinfo($image['name'], PATHINFO_EXTENSION));

		$errors = 0;

		if(!isset($name) || $name == ""){
			$errors++;
		}

		if(!isset($price) || $price < 1){
			$errors++;
		}

		if(!isset($description) || $description == ""){
			$errors++;
		}

		if(!isset($category_id) || $category_id == ""){
			$errors++;
		}

		if($_FILES['image']['size'] > 0 && !in_array($file_ext, $file_types)){
			$errors++;
		}

		if($errors > 0){
			return false;
		}

		else{
			return true;
		}
	}

	if(validateForm()){
		$id = $_POST['id'];
		$name = $_POST['name'];
		$price = $_POST['price'];
		$description = $_POST['description'];
		$category_id = $_POST['category'];
		$imgPath = "";

		$image_query = "SELECT * FROM items WHERE id = $id";

		$image = mysqli_fetch_assoc(mysqli_query($connect, $image_query));
		if($_FILES['image']['name'] == ""){
			$imgPath = $image['imgPath'];
		}

		else{
			$destination = "../assets/images/";
			$file_name = $_FILES['image']['name'];
			
			move_uploaded_file($_FILES['image']['tmp_name'], $destination.$file_name);

			$imgPath = $destination.$file_name;
		}

		$edit_query = "UPDATE items SET name = '$name', price = $price, description = '$description', imgPath = '$imgPath', category_id = $category_id WHERE id = $id";

		$edit_item = mysqli_query($connect, $edit_query);
		
		header("Location: ../views/catalog.php");
	}

	else{
		header("Location: ".$_SERVER['HTTP_REFERER']);
	}
?>