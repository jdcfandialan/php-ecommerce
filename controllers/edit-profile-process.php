<?php  
	require "connection.php";

	function validate_form(){
		$image = $_FILES['display-img'];
		$file_types = ['jpg','jpeg', 'png', 'gif','svg','webp','bmp','tiff','tif'];
		$file_ext = strtolower(pathinfo($image['name'], PATHINFO_EXTENSION));

		$errors = 0;

		if(!isset($image) || $image == ""){
			$errors++;
		}

		else if($_FILES['display-img']['size'] > 0 && !in_array($file_ext, $file_types)){
			$errors++;
		}

		if($errors > 0){
			return false;
		}

		else{
			return true;
		}
	}

	if(validate_form()){
		$user_id = $_POST['user_id'];
		
		$destination = "../assets/images/";
		$image = $_FILES['display-img'];
		$fileName = $image['name'];

		$imgPath = $destination.$fileName;

		move_uploaded_file($image['tmp_name'], $imgPath);

		$upload_image_query = "UPDATE users SET imgPath = '$imgPath' WHERE id = $user_id";

		$new_image = mysqli_query($connect, $upload_image_query);

		header("Location: ../views/profile.php");
	}

	else{
		header("Location: ".$_SERVER['HTTP_REFERER']);
	}
?>