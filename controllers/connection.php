<?php  
	$host = "localhost"; //host name
	$db_username = "root"; //username of the host
	$db_password = ""; //password for the host
	$db_name = "b55ecommerce"; //name of the database

	//establish db connection
	$connect = mysqli_connect($host, $db_username, $db_password, $db_name);

	//check if the connection to the database is a success
	if(!$connect){
		die("Connection failed: ".mysqli_error($connection));
	}
?>