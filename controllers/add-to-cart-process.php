<?php

	session_start();

	$id = $_POST['id'];
	$quantity = $_POST['cart'];

	//process is from cart page
	if(isset($_POST['fromCartPage'])){
		$_SESSION['cart'][$id] = $quantity;
		header("Location: ".$_SERVER['HTTP_REFERER']);
	}

	//process is from catalog page
	else{
		//if SESSION is started, add new cart quantity to old cart quantity
		if(isset($_SESSION['cart'][$id])){
			$_SESSION['cart'][$id] += $quantity;
		}

		//SESSION is not started, create the initial cart value
		else{
			$_SESSION['cart'][$id] = $quantity;
		}
	}
	function getCartSum(){
		return array_sum($_SESSION['cart']);
	}

	echo getCartSum();

?>