<?php  
	require "../partials/template.php";

	function get_title(){
		echo "Order History | Kicks-Dict";
	}

	function get_body_contents(){
?>

	<h1 class="text-center py-5">Order History</h1>

	<div class="container">
		<div class="row">
			<div class="col-lg-8 offset-lg-2">
				<table class=" table table-striped">
					<thead class="thead-dark">
						<tr>
							<th>Order Id</th>
							<th>Item Name</th>
							<th>Amount</th>
						</tr>
					</thead>
					<tbody>
							<?php  
								require "../controllers/connection.php";
								
								$user_id = $_SESSION['user']['id'];

								$order_query = "SELECT * FROM orders WHERE user_id = $user_id";
								$orders =mysqli_query($connect, $order_query);

								foreach($orders as $indiv_order){
							?>
									<tr>
										<td><?php echo $indiv_order['id'] ?></td>
										<td>
											<?php  
												
												$orderId = $indiv_order['id'];
												$items_query = "SELECT * FROM items JOIN item_order ON (items.id = item_order.item_id) WHERE item_order.order_id = $orderId";
												$items = mysqli_query($connect, $items_query);

												foreach($items as $indiv_item){
											?>

													<span><?php echo $indiv_item['name'] ?><br></span>

											<?php
												}
											?>
										</td>
										<td><?php echo $indiv_order['total'] ?></td>
									</tr>
							<?php
								}
							?>

					</tbody>
				</table>
			</div>
		</div>
	</div>

<?php
	}
?>