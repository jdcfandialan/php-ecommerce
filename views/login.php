<?php  
	require "../partials/template.php";

	function get_title(){
		echo "Login | Kicks-Dict";
	}

	function get_body_contents(){
?>

	<h1 class="text-center py-5">Login</h1>
	<div class="col-lg-4 offset-lg-4">
		<form action="../controllers/login-process.php" method="POST">
			<div class="form-group">
				<label for="email">Email:</label>
				<input type="email" name="email" class="form-control" id="email">
				<span class="validation text-danger"></span>
			</div>
			
			<div class="form-group">
				<label for="password">Password:</label>
				<input type="password" name="password" class="form-control" id="password">
				<span class="validation text-danger"></span>
			</div>
			<button type="button" id="loginUser" class="btn btn-info">Login</button>
			<p>Not yet registered? <a href="register.php">Register</a></p>
		</form>
	</div>

	<script type="text/javascript" src="../assets/scripts/login.js"></script>
<?php
	}
?>