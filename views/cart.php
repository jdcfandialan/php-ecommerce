<?php  
	require "../partials/template.php";

	function get_title(){
		echo "Cart | Kicks-Dict";
	}

	function get_body_contents(){
?>

	<h1 class="text-center py-5">My Cart</h1>
	<hr>
	<div class="col-lg-8 offset-lg-2">
		<table class="table table-striped table-bordered">
			<thead>
				<tr class="text-center">
					<th>Item</th>
					<th>Price</th>
					<th>Quantity</th>
					<th>Subtotal</th>
					<th></th>
				</tr>
			</thead>
			<tbody>
				<?php  
					require "../controllers/connection.php";

					$total = 0;

					if(isset($_SESSION['cart'])){
						foreach($_SESSION['cart'] as $itemId => $quantity){
							$item_query = "SELECT * FROM items WHERE id = $itemId";

							$indiv_item = mysqli_fetch_assoc(mysqli_query($connect, $item_query));

							$subtotal = $indiv_item['price'] * $quantity;
							$total += $subtotal;
				?>
							<tr>
								<td class="text-center"><?php echo $indiv_item['name'] ?></td>
								<td class="text-center">USD <span><?php echo $indiv_item['price'] ?></span></td>
								<td><span class="spanQ"><?php echo $quantity ?></span><form action="../controllers/add-to-cart-process.php" method="POST" class="d-none">
									<input type="hidden" name="id" value="<?php echo $itemId ?>">
									<input type="hidden" name="fromCartPage" value="fromCartPage">
									<input type="number" class="form-control" name="cart" style="width:60px" value="<?php echo $quantity ?>" data-id="<?php echo $itemId ?>">
								</form></td>
								<td class="text-right">USD <span><?php echo number_format($subtotal, 2) ?></span></td>
								<td class="text-center"><a href="../controllers/remove-from-cart-process.php?id=<?php echo $itemId ?>" class="btn btn-danger">Remove from Cart</a></td>
							</tr>
				<?php 
						}
					}
				?>
				<tr>
					<td></td>
					<td></td>
					<td class="text-white bg-danger text-right">TOTAL: </td>
					<td class="text-white bg-danger text-right">USD <span id="total"><?php echo number_format($total, 2) ?></span></td>
					<td class="text-center"><a href="../controllers/empty-cart-process.php" class="btn btn-danger">Empty Cart</a></td>
				</tr>
				<tr>
					<td></td>
					<td></td>
					<td>
						<form action="../controllers/checkout-process.php" method="POST">
							<input type="hidden" name="totalPayment" value="<?php echo $total ?>">
							<input type="hidden" name="cod" value="total">
							<button type="submit" class="btn btn-info">Cash-on-Delivery</button>
						</form>
					</td>
					<td><div id="paypal-button-container"></div></td>
					<td></td>
				</tr>
			</tbody>
		</table>
	</div>

	<!-- PAYPAL -->
	<script src="https://www.paypal.com/sdk/js?client-id=AbikR2_5Y9hyKJcS2-phFBl3tB7PrEANm3Wp6U0-z__G3XhU4-cthElbUd4BMSB70XNPmtjPZfMwMObb"></script>

	<script>
		let totalPayment = document.getElementById("total").textContent.split(',').join("");

		paypal.Buttons({
			createOrder: function(data, actions) {
				// This function sets up the details of the transaction, including the amount and line item details.
				return actions.order.create({
					purchase_units: [{
						amount: {
							value: 'totalPayment'
						}
					}]
				});
			},

			onApprove: function(data, actions) {
				// This function captures the funds from the transaction.
				return actions.order.capture().then(function(details) {
					// This function shows a transaction success message to your buyer.
					let data = new FormData;

					data.append("totalPayment", totalPayment);
					data.append("fromPaypal", "fromPaypal");

					fetch("../controllers/checkout-process.php", {
						method: "POST",
						body: data
					}).then(res => res.text())
					.then(res => {
						console.log(res);
						alert('Transaction completed by ' + details.payer.name.given_name);
					});

				});
			}
	}).render('#paypal-button-container');</script>

	<script type="text/javascript" src="../assets/scripts/update-cart.js"></script>
<?php  
	}
?>