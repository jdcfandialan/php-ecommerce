<?php  
	require "../partials/template.php";

	function get_title(){
		echo "My Profile | Kicks-Dict";
	}

	function get_body_contents(){
?>

	<h1 class="text-center py-5">My Profile</h1>
	<div class="container">
		<div class="row">
			<div class="col-lg-6">
				<h3>Profile Details</h3>
				
				<div class="form-group">
					<?php
						require "../controllers/connection.php";
						
						$userId = $_SESSION['user']['id'];
						
						$user_query = "SELECT * FROM users WHERE id = $userId";
						$user_info = mysqli_fetch_assoc(mysqli_query($connect, $user_query));

						$contact_query = "SELECT * FROM contacts WHERE user_id = $userId";
						$contact_info = mysqli_query($connect, $contact_query);

						if($user_info['imgPath'] != NULL){

					?>
							<img src="<?php echo $user_info['imgPath'] ?>" class="img-thumbnail rounded-circle" style="object-fit: contain;height:100px; width:100px" >
					<?php		
						}

						else{
					?>

							<img src="../assets/images/def-img.png" class="img-thumbnail rounded-circle" style="object-fit: contain;height:100px; width:100px">

					<?php		
						}
					?>
					<p>Full Name: <span class="text-danger"><?php echo $_SESSION['user']['firstName']." ".$_SESSION['user']['lastName'] ?></span></p>
					<p>Email: <span class="text-danger "><?php echo $_SESSION['user']['email'] ?></span></p>
					
					<?php  
						foreach($contact_info as $contactNumber){
					?>
							<p>Contact number: <span class="text-danger"><?php echo $contactNumber['contactNo'] ?></span></p>
					<?php		
						}
					?>
					
				</div>
				
				<form action="../controllers/edit-profile-process.php" method="POST" enctype="multipart/form-data">
					<div class="form-group">
						<label for="display-img">Update Display Image:</label>
						<input type="file" name="display-img" class="form-control">
					</div>
					<input type="hidden" name="user_id" value="<?php echo $_SESSION['user']['id'] ?>">
					<button type="submit" class="btn btn-info">Upload Image</button>
				</form>

			</div>

			<div class="col-lg-6">
				<h3>Addresses:</h3>
				
				<ul>
					<?php
						$address_query = "SELECT * FROM addresses WHERE user_id = $userId";

						$addresses = mysqli_query($connect, $address_query);

						foreach($addresses as $indiv_address){
					?>

							<li><?php echo $indiv_address['address1']. ", ".$indiv_address['address2'].", ".$indiv_address['city']. ", ".$indiv_address['zipCode'] ?></li>

					<?php
						}
					?>
				</ul>

				<form action="../controllers/add-address-process.php" method="POST">
					<div class="form-group">
						<label for="address1">Lot Number/House Number/Street Name:</label>
						<input type="text" name="address1" class="form-control">
					</div>
					
					<div class="form-group">
						<label for="address2">City:</label>
						<input type="text" name="address2" class="form-control">
					</div>

					<div class="form-group">
						<label for="city">Province:</label>
						<input type="text" name="city" class="form-control">
					</div>

					<div class="form-group">
						<label for="zipCode">Zip Code:</label>
						<input type="text" name="zipCode" class="form-control">
					</div>
					<input type="hidden" name="user_id" value="<?php echo $userId ?>">
					<button class="btn btn-info">Add Address</button>
				</form>

				<form action="../controllers/add-contact-process.php" method="POST">
					<div class="form-group">
						<label for="contactNumber">Contact Number</label>
						<input type="text" name="contactNumber" class="form-control" placeholder="09XXXXXXXXX">
					</div>
					<input type="hidden" name="user_id" value="<?php echo $userId ?>">
					<button class="btn btn-info"> Add Contact Number</button>
				</form>
			</div>
		</div>
	</div>


<?php		
	}
?>