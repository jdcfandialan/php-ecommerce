<?php  
	require "../partials/template.php";

	function get_title(){
		echo "Catalog | Kicks-Dict";
	}

	function get_body_contents(){
?>
	<h1 class="text-center py-5">LISTINGS</h1>
	<div class="container">
		<div class="row">
			<?php 
				require "../controllers/connection.php";

				//query for getting all the items
				$items_query = "SELECT * FROM items";
				$items = mysqli_query($connect, $items_query);

				foreach($items as $indiv_item){
			?>

					<div class="col-lg-4 py-3">
						<div class="card">
							<img src="<?php echo $indiv_item['imgPath'] ?>" class="card-img-top" style="object-fit: cover" height="200px" alt="broken-img">
							<div class="card-body">
								<h4 class="card-title text-center"><?php echo $indiv_item['name'] ?></h4>
								<p class="card-text">Price: USD <?php echo $indiv_item['price'] ?></p>
								<p class="card-text">Description: <?php echo $indiv_item['description'] ?></p>
								<p class="card-text">Category: 
									<?php  
										$category_id = $indiv_item['category_id'];
										$category_id_query = "SELECT * FROM categories WHERE id = $category_id";
										
										//mysqli_fetch_assoc = turn single row to an associative array
										$category = mysqli_fetch_assoc(mysqli_query($connect, $category_id_query));
										
										if($category['id'] == $category_id){
											echo $category['name'];
										}
									?>
								</p>
							</div>
							<div class="card-footer">
								<a href="../controllers/delete-item-process.php?id=<?php echo $indiv_item['id'] ?>" class="btn btn-danger">Delete</a>
								<a href="edit-item.php?id=<?php echo $indiv_item['id'] ?>" class="btn btn-info">Edit</a>
							</div>
							<div class="card-footer">
								<!-- <form class="form-group" action="../controllers/add-to-cart-process.php" method="POST">
									<input type="hidden" name="id" value="<?php //echo $indiv_item['id'] ?>"> -->
									<input type="number" name="cart" class="form-control" value="1">
									<button type="button" class="btn btn-primary addToCart my-1" data-id="<?php echo $indiv_item['id'] ?>">Add to Cart</button>
								<!-- </form> -->
							</div>	
						</div>
					</div>

			<?php  
				}
			?>
		</div>
	</div>

	<script type="text/javascript" src="../assets/scripts/add-to-cart.js"></script>
<?php  
	}
?>