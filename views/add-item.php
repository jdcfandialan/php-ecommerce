<?php  
	require "../partials/template.php";

	function get_title(){
		echo "Add an item | Kicks-Dict";
	}

	function get_body_contents(){
?>

	<h1 class="text-center py-5">ADD ITEM</h1>
	<div class="container">
		<div class="col-lg-6 offset-lg-3">
			<form action="../controllers/add-item-process.php" method="POST" enctype="multipart/form-data">
				<div class="form-group">
					<label for="name">Item name: </label>
					<input type="text" name="name" class="form-control">
				</div>

				<div class="form-group">
					<label for="price">Item price: </label>
					<input type="number" name="price" class="form-control">
				</div>

				<div class="form-group">
					<label for="description">Item description: </label>
					<textarea style="resize: none" name="description" class="form-control"></textarea>
				</div>

				<div class="form-group">
					<label for="image">Item image: </label>
					<input type="file" name="image" class="form-control">
				</div>
				<div class="form-group">
					<label for="category">Item category: </label>
					<select name="category" class="form-control">
						<option value="" disabled selected>Select an item category</option>
						<?php 
							require "../controllers/connection.php";

							$category_query = "SELECT * FROM categories";
							$categories = mysqli_query($connect, $category_query);

							foreach($categories as $indiv_category){
						?>

								<option value="<?php echo $indiv_category['id'] ?>"><?php echo $indiv_category['name'] ?></option>

						<?php  
							}
						?>
					</select>
				</div>
				<div class="text-center">
					<button class="btn btn-info" type="submit">Add Item</button>
				</div>
			</form>
		</div>
	</div>

<?php  
	}
?>